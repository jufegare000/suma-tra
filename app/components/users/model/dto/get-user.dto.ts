export interface GetUserDTO {
    id: string;
    email: string;
    role: string;
}