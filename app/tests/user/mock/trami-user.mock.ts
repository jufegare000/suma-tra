import { UserI } from "../../../components/users/model/interfaces/tramiUser.interface"; 

export const tramiUserTramitadorMock: UserI 
    = 
        {
            email: 'tramitador@tramisama.tr',
            role: 'tramitador',
            id: 1,
            pass: '12313'
        };

export const tramiUserSolicitanteMock: UserI 
    = 
        {
            email: 'solicitante@tramisama.tr',
            role: 'solicitante',
            id: 2,
            pass: '12313'
        };


